package poc.shell.util.image;

import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;
import android.graphics.drawable.PictureDrawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;

import poc.shell.R;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;


public class ImagesManager {

    public static void loadImg(Context context, String url, ImageView imageView) {
        Glide.with(context).load(url)
                .thumbnail(Glide.with(context).load(R.drawable.loader))
                .into(imageView);
    }

    public static void loadSVGImage(Context context, String url, ImageView imageView){
        RequestBuilder<PictureDrawable> requestBuilder = Glide.with(context)
                .as(PictureDrawable.class)
                .transition(withCrossFade())
                .listener(new SvgSoftwareLayerSetter());
        requestBuilder.load(url).into(imageView);
    }
}

