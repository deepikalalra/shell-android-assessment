package poc.shell.util.ui;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import poc.shell.R;


public class LoadingContentContainer extends RelativeLayout {

    private static final String DEFAULT_EMPTY_IMAGE = "";
    private RelativeLayout contentContainer;
    private ContentLoadingProgressBar contentLoadingProgressBar;
    private RelativeLayout blurView;

    public LoadingContentContainer(Context context) {
        super(context);
    }

    public LoadingContentContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.custom_loading_content_container, this, true);
        // hold a reference of the container where the children to be placed
        contentContainer = findViewById(R.id.content_container);
        contentLoadingProgressBar = findViewById(R.id.content_loader);
        blurView = findViewById(R.id.blur_view);
    }


    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (contentContainer == null) {
            super.addView(child, index, params);
        } else {
            //Forward these calls to the content view
            contentContainer.addView(child, index, params);
        }
    }

    private void showProgress() {
        blurView.setVisibility(View.VISIBLE);
        blurView.setBackgroundColor(Color.TRANSPARENT);
        contentLoadingProgressBar.setVisibility(VISIBLE);
    }

    private void showProgressWithBlur() {
        blurView.setVisibility(View.VISIBLE);
        blurView.setBackgroundColor(Color.LTGRAY);
        blurView.setAlpha(0.6f);
        contentLoadingProgressBar.setVisibility(VISIBLE);
    }

    public void hideProgress() {
        contentLoadingProgressBar.hide();
        blurView.setVisibility(View.GONE);
    }

    public void showContent() {
        if (!contentContainer.isShown())
            contentContainer.setVisibility(View.VISIBLE);
    }

    public void hideContent() {
        if (contentContainer.isShown())
            contentContainer.setVisibility(View.GONE);
    }

    public void hideContentShowProgress() {
        hideContent();
        showProgress();
    }

    public void showContentHideProgress() {
        hideProgress();
        showContent();
    }

}
