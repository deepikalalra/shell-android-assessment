package poc.shell.util.ui;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import poc.shell.R;


public class AlertUtil {

    public static void showSnackBar(Activity activityContext, String message) {
        Snackbar snackbar = Snackbar.make(activityContext.findViewById(android.R.id.content),
                message, Snackbar.LENGTH_LONG);
        View view = snackbar.getView();
        TextView textView = view.findViewById(R.id.snackbar_text);
        view.setBackgroundColor(ContextCompat.getColor(activityContext, R.color.colorPrimary));
        textView.setTextColor(ContextCompat.getColor(activityContext, R.color.white));
        textView.setMaxLines(2);
        textView.setGravity(Gravity.CENTER);
        snackbar.show();
    }

}
