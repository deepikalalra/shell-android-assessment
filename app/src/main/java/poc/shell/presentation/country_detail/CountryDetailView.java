package poc.shell.presentation.country_detail;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import poc.shell.R;
import poc.shell.data.model.Country;
import poc.shell.presentation.base.BaseMvvmView;
import poc.shell.util.image.ImagesManager;

public class CountryDetailView extends BaseMvvmView implements LifecycleOwner {

    @BindView(R.id.image_view_flag)
    ImageView flagImageView;

    @BindView(R.id.text_view_country_name)
    TextView countryNameTextView;

    @BindView(R.id.text_view_capital_name)
    TextView capitalNameTextView;

    @BindView(R.id.text_view_calling_code)
    TextView callingCodeTextView;

    @BindView(R.id.text_view_region)
    TextView regionTextView;

    @BindView(R.id.text_view_sub_region)
    TextView subRegionTextView;

    @BindView(R.id.text_view_time_zone)
    TextView timeZoneTextView;

    @BindView(R.id.text_view_curriencies)
    TextView currenciesTextView;

    @BindView(R.id.text_view_languages)
    TextView langTextView;

    private Country selectedCountry;


    private CountryDetailViewModel viewModel;

    public CountryDetailView(@NonNull Activity context, Country selectedCountry) {
        super(context, R.layout.fragment_country_detail);
        this.selectedCountry = selectedCountry;
        renderUi();
    }

    @Override
    protected void initViews() {
        viewModel = new CountryDetailViewModel(getWeakReferenceContext(context));
    }

    @Override
    protected void initDI() {
        //add DI here
    }

    @Override
    protected View getView() {
        return view;
    }

    private void renderUi() {
        if (selectedCountry != null) {
            ImagesManager.loadSVGImage(context, selectedCountry.getFlag(), flagImageView);
            countryNameTextView.setText(selectedCountry.getName());
            capitalNameTextView.setText(selectedCountry.getCapital());
            callingCodeTextView.setText(selectedCountry.getFormattedCallingCodes());
            regionTextView.setText(selectedCountry.getRegion());
            subRegionTextView.setText(selectedCountry.getSubregion());
            timeZoneTextView.setText(selectedCountry.getFormattedTimeZones());
            currenciesTextView.setText(selectedCountry.getFormattedCurrencies());
            langTextView.setText(selectedCountry.getFormattedLanguages());
        }
    }

    @OnClick(R.id.button_save)
    void onSaveButtonClicked() {

    }

}
