package poc.shell.presentation.country_detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import poc.shell.data.model.Country;
import poc.shell.presentation.base.BaseMvvmFragment;

public class CountryDetailFragment extends BaseMvvmFragment {
    public static final String BUNDLE_KEY_SELECTED_COUNTRY = "Selected_country";
    private CountryDetailView view;
    private Country selectedCountry;


    public static CountryDetailFragment newInstance(Country country) {
        CountryDetailFragment fragment = new CountryDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BUNDLE_KEY_SELECTED_COUNTRY, country);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void getDataFromBundle() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            selectedCountry = (Country) bundle.getSerializable(BUNDLE_KEY_SELECTED_COUNTRY);
        }
    }

    @Override
    protected View bindView(LayoutInflater inflater, ViewGroup container) {
        view = new CountryDetailView(getActivity(), selectedCountry);
        return view.getView();
    }

}
