package poc.shell.presentation.country_detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import poc.shell.R;
import poc.shell.data.model.Country;
import poc.shell.presentation.base.BaseActivity;
import poc.shell.util.ActivityUtils;


public class CountryDetailActivity extends BaseActivity {

    private Country selectedCountry;

    public static Intent getStartIntent(Context context, Country country) {
        Intent intent = new Intent(context, CountryDetailActivity.class);
        intent.putExtra(CountryDetailFragment.BUNDLE_KEY_SELECTED_COUNTRY, country);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_detail);

        CountryDetailFragment selectionFragment = (CountryDetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.activity_country_detail_content_frame);

        if (selectionFragment == null) {
            selectedCountry = (Country) this.getIntent().getExtras().get(CountryDetailFragment.BUNDLE_KEY_SELECTED_COUNTRY);
            selectionFragment = CountryDetailFragment.newInstance(selectedCountry);

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    selectionFragment, R.id.activity_country_detail_content_frame);
        }
    }


}