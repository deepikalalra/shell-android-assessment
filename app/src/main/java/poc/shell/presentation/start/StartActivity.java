package poc.shell.presentation.start;

import android.app.Activity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;
import poc.shell.R;
import poc.shell.presentation.countries.CountriesActivity;


public class StartActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.activity_start_button_start)
    void onStartButtonClicked() {
        startActivity(CountriesActivity.getStartIntent(this));
    }

}