package poc.shell.presentation.countries;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import poc.shell.presentation.base.BaseMvvmFragment;

public class CountriesFragment extends BaseMvvmFragment {
    private CountriesView view;

    public static CountriesFragment newInstance() {
        CountriesFragment fragment = new CountriesFragment();
        return fragment;
    }

    @Override
    protected View bindView(LayoutInflater inflater, ViewGroup container) {
        view = new CountriesView(getActivity());
        return view.getView();
    }

}
