package poc.shell.presentation.countries;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.jetbrains.annotations.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import poc.shell.R;


public class CountryViewHolder extends RecyclerView.ViewHolder {

    @Nullable
    @BindView(R.id.item_list_layout_container)
    RelativeLayout cellContainer;

    @Nullable
    @BindView(R.id.item_list_country_flag)
    ImageView flagImageView;

    @Nullable
    @BindView(R.id.item_list_country_name)
    TextView countryNameTextView;


    public CountryViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}