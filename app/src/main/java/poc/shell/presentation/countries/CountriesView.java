package poc.shell.presentation.countries;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import java.util.List;

import butterknife.BindView;
import butterknife.OnTextChanged;
import poc.shell.R;
import poc.shell.data.model.Country;
import poc.shell.presentation.base.BaseMvvmView;
import poc.shell.util.ui.AlertUtil;
import poc.shell.util.ui.LoadingContentContainer;

public class CountriesView extends BaseMvvmView implements LifecycleOwner {

    @BindView(R.id.loading_content_container)
    LoadingContentContainer loadingContentContainer;

    @BindView(R.id.fragment_countries_search_edit_text)
    EditText searchEditText;

    @BindView(R.id.fragment_countries_recycler_view)
    RecyclerView recyclerView;

    private CountriesAdapter adapter;
    private List<Country> countries;


    private CountriesViewModel viewModel;

    public CountriesView(@NonNull Activity context) {
        super(context, R.layout.fragment_countries);
    }

    @Override
    protected void initViews() {
        viewModel = new CountriesViewModel(getWeakReferenceContext(context));
        observeProgressData();
        observeNetworkErrorData();
        observeUpdateUiData();

        //hide loader for first time
        loadingContentContainer.showContentHideProgress();
    }

    @Override
    protected void initDI() {
        //add DI here
    }

    @Override
    protected View getView() {
        return view;
    }


    private void observeProgressData() {
        viewModel.getProgressData().observe(this, isShown -> {
            if (isShown != null && isShown) {
                loadingContentContainer.hideContentShowProgress();
            } else {
                loadingContentContainer.showContentHideProgress();
            }
        });
    }

    private void observeNetworkErrorData() {
        viewModel.getNetworkErrorLiveData().observe(this, isNetworkError -> {
            AlertUtil.showSnackBar(context, getContext().getString(R.string.message_network_error));
        });
    }

    private void observeUpdateUiData() {
        viewModel.getCountriesLiveData().observe(this, countries -> {
            this.countries = countries;
           setListAdapter();
        });
    }

    private void setListAdapter(){
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new CountriesAdapter(context, countries);
        recyclerView.setAdapter(adapter);
    }

    @OnTextChanged(R.id.fragment_countries_search_edit_text)
    void onSearchTextChange(){
        viewModel.searchCountries(searchEditText.getText().toString());
    }

}
