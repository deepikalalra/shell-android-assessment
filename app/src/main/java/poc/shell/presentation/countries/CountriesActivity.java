package poc.shell.presentation.countries;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import poc.shell.R;
import poc.shell.presentation.base.BaseActivity;
import poc.shell.util.ActivityUtils;


public class CountriesActivity extends BaseActivity {


    public static Intent getStartIntent(Context context) {
        return new Intent(context, CountriesActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries);

        CountriesFragment selectionFragment = (CountriesFragment) getSupportFragmentManager()
                .findFragmentById(R.id.activity_countries_content_frame);

        if (selectionFragment == null) {
            selectionFragment = CountriesFragment.newInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    selectionFragment, R.id.activity_countries_content_frame);
        }

    }
}