package poc.shell.presentation.countries;

import android.app.Activity;
import android.arch.lifecycle.LiveData;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Stack;

import poc.shell.data.callback.CountryCallBack;
import poc.shell.data.model.Country;
import poc.shell.data.service.CountryService;
import poc.shell.data.service.remote.IHttpConstant;
import poc.shell.presentation.base.BaseViewModel;
import poc.shell.presentation.base.SingleLiveEvent;


public class CountriesViewModel extends BaseViewModel {
    protected final SingleLiveEvent<ArrayList<Country>> countriesLiveData = new SingleLiveEvent<>();
    private WeakReference<Activity> activityWeakReference;

    public LiveData<ArrayList<Country>> getCountriesLiveData() {
        return countriesLiveData;
    }

    private Stack<String> searchKeywordStack;
    private String currentKeywordSearched;

    public CountriesViewModel(WeakReference<Activity> activityWeakReference) {
        super();
        this.activityWeakReference = activityWeakReference;
        this.searchKeywordStack = new Stack<>();
    }

    void searchCountries(String searchKeyword) {
        if (this.searchKeywordStack.isEmpty()) {
            this.searchKeywordStack.add(searchKeyword);
            //also search for network.
            searchCountriesFromNetwork();
        } else {
            //just add the keyword. later when response callback comes we will hit the api again
            this.searchKeywordStack.add(searchKeyword);
        }
    }

    private void searchCountriesFromNetwork() {
        currentKeywordSearched = this.searchKeywordStack.peek();
        CountryService countryService = new CountryService(activityWeakReference, catalogueCallBack, currentKeywordSearched);
        countryService.execute();
        progressData.postValue(true);
    }

    public CountryCallBack catalogueCallBack = new CountryCallBack() {
        @Override
        public void onCountrySearchSuccess(ArrayList<Country> countries, int statusCode) {
            if (isContextAvailable(activityWeakReference)) {
                switch (statusCode) {
                    case IHttpConstant.HTTP_200:
                        countriesLiveData.postValue(countries);
                        checkForNextSearch();
                        break;
                    default:
                        checkForNextSearch();
                        networkErrorLiveData.postValue(true);
                }
                progressData.postValue(false);
            }
        }

        @Override
        public void onCountrySearchError(String error) {
            if (isContextAvailable(activityWeakReference)) {
                networkErrorLiveData.postValue(true);
                progressData.postValue(false);
                checkForNextSearch();
            }
        }
    };

    private void checkForNextSearch(){
        String topKeyword = !searchKeywordStack.isEmpty() ? searchKeywordStack.peek() : null;
        if (topKeyword != null && !topKeyword.equalsIgnoreCase(currentKeywordSearched)) {
            //search the latest keyword
            searchCountriesFromNetwork();
        }
        //clear the stack.
        searchKeywordStack.clear();
    }


}
