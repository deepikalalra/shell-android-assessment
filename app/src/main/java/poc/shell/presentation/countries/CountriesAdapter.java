package poc.shell.presentation.countries;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import poc.shell.R;
import poc.shell.data.model.Country;
import poc.shell.presentation.country_detail.CountryDetailActivity;
import poc.shell.util.image.ImagesManager;

public class CountriesAdapter extends RecyclerView.Adapter<CountryViewHolder> {

    private Activity context;
    private List<Country> countries;

    public CountriesAdapter(Activity context, List<Country> countries) {
        this.context = context;
        this.countries = countries;
    }

    @NonNull
    @Override
    public CountryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_country, parent, false);
        return new CountryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryViewHolder viewHolder, final int position) {
        Country country = countries.get(position);
        ImagesManager.loadSVGImage(context, country.getFlag(), viewHolder.flagImageView);
        viewHolder.countryNameTextView.setText(country.getName());
        viewHolder.cellContainer.setTag(position);

        viewHolder.cellContainer.setOnClickListener(view -> {
            Country selectedCountry = countries.get((int)view.getTag());
            context.startActivity(CountryDetailActivity.getStartIntent(context, selectedCountry));
        });
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

}
