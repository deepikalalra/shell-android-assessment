package poc.shell.presentation.base;

import android.app.Activity;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.FrameLayout;

import java.lang.ref.WeakReference;

import butterknife.ButterKnife;
import poc.shell.presentation.base.BaseActivity;


public abstract class BaseMvvmView extends FrameLayout implements LifecycleOwner {

    protected View view;
    protected Activity context;

    public BaseMvvmView(@NonNull Activity context, int resId) {
        super(context);
        this.context = context;
        view = inflate(context, resId, this);
        ButterKnife.bind(this, view);
        initDI();
        initViews();

    }

    //over-ride this method in child views
    protected abstract void initDI();

    protected abstract View getView();

    protected void initViews() {
    }

    protected BaseActivity getBaseActivityContext() {
        return ((BaseActivity) context);
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return ((BaseActivity) context).getLifecycle();
    }

    public WeakReference<Activity> getWeakReferenceContext(Activity activity) {
        return new WeakReference<Activity>(activity);
    }
}
