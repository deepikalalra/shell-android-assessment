package poc.shell.presentation.base;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.lang.ref.WeakReference;


public class BaseViewModel extends ViewModel {
    protected final SingleLiveEvent<Boolean> networkErrorLiveData = new SingleLiveEvent<>();
    public final MutableLiveData<Boolean> progressData = new MutableLiveData<>();

    public LiveData<Boolean> getNetworkErrorLiveData() {
        return networkErrorLiveData;
    }
    public LiveData<Boolean> getProgressData() {
        return progressData;
    }

    protected boolean isContextAvailable(WeakReference<Activity> activityWeakReference) {
        return (activityWeakReference != null
                && activityWeakReference.get() != null
                && !activityWeakReference.get().isFinishing()
                && !activityWeakReference.get().isDestroyed());
    }
}
