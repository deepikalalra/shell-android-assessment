package poc.shell.data.callback;


import java.util.ArrayList;

import poc.shell.data.model.Country;

public interface CountryCallBack {

    void onCountrySearchSuccess(ArrayList<Country> countryResponse, int statusCode);

    void onCountrySearchError(String error);
}
