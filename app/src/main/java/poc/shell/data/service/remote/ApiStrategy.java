package poc.shell.data.service.remote;

public interface ApiStrategy {
    //Bitmask for Network and DB options.
    int REQUEST = 0x000001;
    int STORE = 0x000010;
    int GET_CACHE = 0x000100;

}
