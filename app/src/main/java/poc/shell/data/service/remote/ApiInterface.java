package poc.shell.data.service.remote;

import java.util.ArrayList;

import poc.shell.data.model.Country;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface ApiInterface {

    @GET("name/{searchKeyword}")
    Call<ArrayList<Country>> searchCountry(@Path("searchKeyword") String searchKeyword);

}
