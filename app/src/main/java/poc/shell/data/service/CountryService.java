package poc.shell.data.service;

import android.app.Activity;
import android.support.annotation.NonNull;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import poc.shell.data.callback.CountryCallBack;
import poc.shell.data.model.Country;
import poc.shell.data.service.remote.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CountryService extends BaseService {
    private CountryCallBack countryCallBack;
    private String searchKeyword;

    public CountryService(WeakReference<Activity> activity, CountryCallBack countryCallBack, String searchKeyword) {
        this.countryCallBack = countryCallBack;
        this.activityContext = activity;
        this.searchKeyword = searchKeyword;
    }

    @Override
    protected void executeNetworkRequest(ApiInterface apiService) {

        Call<ArrayList<Country>> call = apiService.searchCountry(searchKeyword);
        call.enqueue(new Callback<ArrayList<Country>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<Country>> call, @NonNull Response<ArrayList<Country>> response) {
                if (isContextAvailable()) {
                    countryCallBack.onCountrySearchSuccess(response.body(), response.code());
                }
                checkForOfflineStorage();
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Country>> call, @NonNull Throwable throwable) {
                handleNetworkError(throwable.toString());
            }
        });
    }

    @Override
    protected void initDataFromDatabase() {
        //Right now I am not implementing offline caching here.
        // But its always good to provide user cached data first. (if we can)
    }

    @Override
    protected void handleNetworkError(String errorMessage) {
        //if we have implemented cached data handling.
        // We can return cached data here to the views to display in case no-network
        //just display network error message
        countryCallBack.onCountrySearchError(null);
    }

    @Override
    protected void storeDataInDatabase() {
        //store fresh data that we have just recd from network for caching.
        // We can store this data in local DB using Room. For now I am not implementing Offline caching here
    }
}
