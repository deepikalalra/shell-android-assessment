package poc.shell.data.service;

import android.app.Activity;

import java.lang.ref.WeakReference;


import poc.shell.data.service.remote.ApiInterface;
import poc.shell.data.service.remote.ApiStrategy;
import poc.shell.data.service.remote.RequestManager;
import poc.shell.util.network.NetworkUtils;
import timber.log.Timber;

/**
 * This base service class will decide weather to call DB or Network for the data.
 * This is basically decision maker also here.
 */
public abstract class BaseService {
    protected static int SUCCESS_RESPONSE_CODE = 200;

    protected int apiStrategy;
    protected WeakReference<Activity> activityContext;
    protected String appDomain;
    protected String locale;

    public BaseService() {
    }

    /**
     * These method we will use to enhance this POC project for offline support.
     * For now I am not using this anywhere.
     *
     * @return
     */
    public int getApiStrategy() {
        return apiStrategy;
    }

    /**
     * These method we will use to enhance this POC project for offline support.
     * For now I am not using this anywhere.
     *
     * @return
     */
    public void setApiStrategy(int apiStrategy) {
        this.apiStrategy = apiStrategy;
    }

    /**
     * This method will decide weather to call network or pass the data through offline storage.
     * For now as we are not using APIStrategy anywhere this method will always request network.
     */
    public void execute() {
        if ((apiStrategy & ApiStrategy.GET_CACHE) != 0) {
            Timber.i("Get DB data");
            initDataFromDatabase();
        }

        if (isContextAvailable()) {
            if (NetworkUtils.isNetworkConnected(activityContext.get())) {
                ApiInterface apiService = RequestManager.getClient().create(ApiInterface.class);
                executeNetworkRequest(apiService);
            } else {
                //only show network error if we don't want offline data
                handleNetworkError(null);
            }
        }
    }

    protected void checkForOfflineStorage() {
        if ((apiStrategy & ApiStrategy.STORE) != 0) {
            Timber.i("Store DB data");
            storeDataInDatabase();
        }

    }

    /**
     * As we will be using weak context so its always good to check if activity or fragment is live or not.
     * Else this will throw you windowBadToken exception if we try to use main thread of already
     * killed activities or fragments
     *
     * @return
     */
    protected boolean isContextAvailable() {
        return (activityContext != null
                && activityContext.get() != null
                && !activityContext.get().isFinishing()
                && !activityContext.get().isDestroyed());
    }

    //override this method to execute network request
    abstract protected void executeNetworkRequest(ApiInterface apiService);

    //override this method to get data from database
    abstract protected void initDataFromDatabase();

    //override this method to handle network error
    abstract protected void handleNetworkError(String errorMessage);

    //override this method to store data in database.
    abstract void storeDataInDatabase();

}
